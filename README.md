# Conjuntos, Aplicaciones y funciones (2002)
```plantuml
@startmindmap
* Conjuntos, Aplicaciones y funciones (2002)
** que es? 
*** operaciones que pueden efectuarse entre ellos
*** relaciones entre ellos 
****_ es decir 
*****  su unión
***** intersección
***** complemento 
***** otro
*** agrupación de elementos
****_ ya sean 
***** números
***** letras
***** palabras
***** funciones
***** símbolos
***** figuras geométricas
***** otros 
** Conjuntos numéricos
***_  son
**** agrupaciones
****_ donde
***** clasifican los números
** Álgebra de conjuntos
***_ es?
**** relaciones entre ellos
***_ son?
**** Unión de conjuntos
*****_ es
****** unión de dos o más conjuntos 
**** Intersección de conjuntos
*****_ es
****** intersección de dos o más conjuntos
**** Diferencia de conjuntos
*****_ es
****** diferencia de un conjunto respecto a otro
**** Conjuntos complementarios
*****_ es 
****** complemento de un conjunto
**** Diferencia simétrica
*****_ es
****** diferencia de dos conjuntos de todos \nelementos que están \nen uno o en otro
**** Producto cartesiano
*****_ es
****** resultado un nuevo conjunto
@endmindmap
```

# Funciones (2010)
```plantuml
@startmindmap
* Funciones (2010)
**_ es
*** es la relación que hay entre una magnitud y otra 
** variables
*** variable dependiente
*** variable independiente
** Tipos de funciones matemáticas
***_ son
**** Función inyectiva
*****_ es
****** relación matemática entre dominio y codominio
**** Función sobreyectiva
*****_ es
****** valores del codominio (y) están relacionados con al menos uno del dominio (x)
**** Función biyectiva
*****_ es
****** propiedades tanto inyectivas como suryectivas
**** Funciones algebraicas
*****_ son
******  relación de componentes monomios o polinomios
**** Funciones polinómicas
**** Funciones a trozos
*****_ son
****** el valor de y cambia el comportamiento de la función
**** Funciones racionales
**** Funciones radicales
*****_ son
****** aparece introducida dentro de un radical o raíz
**** Funciones trascendentes
*****_ son
****** relaciones no pueden obtenerse a través de operaciones algebraicas
**** no inyectivas y no suryectivas
*****_ son
****** indican que existen múltiples valores del dominio para un codominio concreto 
@endmindmap
```
# La matemática del computador (2002)
```plantuml
@startmindmap
* La matemática del computador (2002)
**  comprende
*** investigación matemática
*** conglomerado de programas
** usos
*** ciencias 
*** algoritmos 
** algoritmos y teorías
***_ resuelven
**** ordenador
** área aplicadas
***_ son 
**** Ciencias computacionales
*****_ conocido como
****** computación científica
*******_ implica
******** pruebas matemáticas
******** análisis numérico
******** teoría de los métodos numéricos
****** ingeniería computacional 
**** Métodos numéricos
**** Métodos estocásticos
*****_ como los 
****** métodos de Monte Carlo
****** la incertidumbre en la computación científica
**** Análisis numéricos
**** Sistemas de álgebra
**** Geometría algebraica computacional
**** Criptografía
**** Estadística computacional
** profesionales
***_ deben de saber
**** desarrollar nuevos algoritmos
**** herramientas para la optimización
**** modelización
**** tratamiento de datos complejos 
@endmindmap
```
